FROM openjdk:17-jdk

VOLUME /tmp


COPY authentication-ban.jar authentication-ban.jar

ENTRYPOINT ["java","-jar","/authentication-ban.jar"]
