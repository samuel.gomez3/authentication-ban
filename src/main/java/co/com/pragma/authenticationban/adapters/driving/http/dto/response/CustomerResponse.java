package co.com.pragma.authenticationban.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CustomerResponse {
	
	
	
	private long id;
	private String fullName;
	private String country;
	private Double income;
	private String city;
	private int age;

}
