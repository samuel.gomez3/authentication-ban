package co.com.pragma.authenticationban.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CustomerRegistrationRequestDto {
	@NotBlank
	private String fullName;
	@NotBlank
	private String key;
	@NotBlank
	private String country;
	

}
