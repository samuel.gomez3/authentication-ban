package co.com.pragma.authenticationban.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class LoginRequestDto {
	@NotBlank
	private String username;
	@NotBlank
	private String password;

}
