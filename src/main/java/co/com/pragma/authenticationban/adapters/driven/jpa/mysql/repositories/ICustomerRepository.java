package co.com.pragma.authenticationban.adapters.driven.jpa.mysql.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities.CustomerEntity;

public interface ICustomerRepository extends JpaRepository<CustomerEntity, Long> {

	List<CustomerEntity> findById(long id);

}
