package co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table (name = "BusinessUser")
public class BusinessUserEntity {
	
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	String name;
	String username;
	String password;

}
