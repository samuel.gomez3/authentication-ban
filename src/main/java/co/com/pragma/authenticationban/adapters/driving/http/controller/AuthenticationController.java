package co.com.pragma.authenticationban.adapters.driving.http.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import co.com.pragma.authenticationban.adapters.driving.http.dto.request.*;

import co.com.pragma.authenticationban.adapters.driving.http.handlers.*;
import co.com.pragma.authenticationban.configuration.*;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
	private static final String BUSINESS_ROLE = "business";

	private final IAuthenticationHandler authenticationHandler;

	@PostMapping("/login")
	public ResponseEntity<String> login(@Valid @RequestBody LoginRequestDto loginRequestDto) {
		logger.info("Received login request");
		String response = authenticationHandler.login(loginRequestDto);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/customerRegistration")
	public ResponseEntity<String> customerRegistration(
			@Valid @RequestBody CustomerRegistrationRequestDto customerRegistrationRequestDto) {
		logger.info("Received customer registration request");
		authenticationHandler.customerRegistration(customerRegistrationRequestDto);
		return new ResponseEntity<>("Your registration was successful", HttpStatus.OK);
	}

	@GetMapping("/customers")
	public ResponseEntity<?> getCustomers(@RequestHeader("token") String token) {
		logger.info("Received get customers request");
		String role = JwtUtil.validateToken(token).get("rol", String.class);
		if (!BUSINESS_ROLE.equalsIgnoreCase(role)) {
			return new ResponseEntity<>("Unauthorized access", HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<>(authenticationHandler.getCustomers(), HttpStatus.OK);
	}

	@GetMapping("/customer/{id}")
	public ResponseEntity<?> getCustomersById(@RequestHeader("token") String token, @PathVariable Long id) {
		logger.info("Received get customer by id request");
		String role = JwtUtil.validateToken(token).get("rol", String.class);
		if (!BUSINESS_ROLE.equalsIgnoreCase(role)) {
			return new ResponseEntity<>("Unauthorized access", HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<>(authenticationHandler.getCustomers(id), HttpStatus.OK);
	}


	@PatchMapping("/updateCustomerInformation/{id}")
	public ResponseEntity<String> updateCustomerInformation(
			@Valid @RequestBody CustomerUpdateInformationRequestDto customerUpdateInformationRequestDto,
			@RequestHeader("token") String token, @PathVariable Long id) {
		logger.info("Received update customer information request");
		String role = JwtUtil.validateToken(token).get("rol", String.class);
		if (!BUSINESS_ROLE.equalsIgnoreCase(role)) {
			return new ResponseEntity<>("Unauthorized access", HttpStatus.UNAUTHORIZED);
		}
		authenticationHandler.updateCustomerInformation(customerUpdateInformationRequestDto, id);
		return new ResponseEntity<>("The data was updated correctly", HttpStatus.OK);
	}
}
