package co.com.pragma.authenticationban.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CustomerUpdateInformationRequestDto {
	private double income;
	@NotBlank
	private String city;

	private int age;

}
