package co.com.pragma.authenticationban.adapters.driven.jpa.mysql.mappers;

import java.util.List;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities.CustomerEntity;
import co.com.pragma.authenticationban.domain.model.Customer;

public interface IAuthenticationEntityMapper {

	CustomerEntity toEntity(Customer customer);
	List<Customer> toModelList(List<CustomerEntity> customerEntities);
	Customer toModel(CustomerEntity customerEntity);
}
