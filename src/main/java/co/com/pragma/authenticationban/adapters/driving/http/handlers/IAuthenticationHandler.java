package co.com.pragma.authenticationban.adapters.driving.http.handlers;

import java.util.List;

import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerRegistrationRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerUpdateInformationRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.request.LoginRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.response.CustomerResponse;


public interface IAuthenticationHandler {

	String login(LoginRequestDto loginRequestDto);
	
	void customerRegistration(CustomerRegistrationRequestDto customerRegistrationRequestDto);
	
	void updateCustomerInformation(CustomerUpdateInformationRequestDto updateInformationRequestDto, long id);
	
	List<CustomerResponse> getCustomers();
	
	List<CustomerResponse> getCustomers(long id);

}
