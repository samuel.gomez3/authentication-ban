package co.com.pragma.authenticationban.adapters.driven.jpa.mysql.mappers.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities.CustomerEntity;
import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.mappers.IAuthenticationEntityMapper;
import co.com.pragma.authenticationban.domain.model.Customer;

@Component
public class AuthenticationEntityMapper implements IAuthenticationEntityMapper {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationEntityMapper.class);

	@Override
	public CustomerEntity toEntity(Customer customer) {
		if (customer == null) {
			logger.warn("Input Customer object is null");
			return null;
		}

		logger.info("Mapping Customer to CustomerEntity");
		return new CustomerEntity(customer.getId(), customer.getFullName(), customer.getKey(), customer.getCountry(),
				customer.getIncome(), customer.getCity(), customer.getAge());
	}

	@Override
	public List<Customer> toModelList(List<CustomerEntity> customerEntities) {
		if (customerEntities == null || customerEntities.isEmpty()) {
			logger.warn("Input CustomerEntity list is null or empty");
			return List.of();
		}

		logger.info("Mapping List<CustomerEntity> to List<Customer>");
		return customerEntities.stream().map(this::toModel).collect(Collectors.toList());
	}

	@Override
	public Customer toModel(CustomerEntity customerEntity) {
		if (customerEntity == null) {
			logger.warn("Input CustomerEntity object is null");
			return null;
		}

		logger.info("Mapping CustomerEntity to Customer");
		return new Customer(customerEntity.getId(), customerEntity.getFullName(), customerEntity.getCustomer_key(),
				customerEntity.getCountry(), customerEntity.getIncome(), customerEntity.getCity(),
				customerEntity.getAge());
	}
}
