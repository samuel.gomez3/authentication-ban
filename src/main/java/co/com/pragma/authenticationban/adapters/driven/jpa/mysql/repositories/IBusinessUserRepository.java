package co.com.pragma.authenticationban.adapters.driven.jpa.mysql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities.BusinessUserEntity;

public interface IBusinessUserRepository extends JpaRepository<BusinessUserEntity, Long> {
	BusinessUserEntity findByUsername(String username);
}
