package co.com.pragma.authenticationban.adapters.driving.http.mappers;

import java.util.List;

import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerRegistrationRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerUpdateInformationRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.response.CustomerResponse;
import co.com.pragma.authenticationban.domain.model.Customer;

public interface IAutherizationRequestMapper {
	
	Customer toCustomer (CustomerRegistrationRequestDto customerRegistrationRequestDto) ;
	Customer toCustomer (CustomerUpdateInformationRequestDto customerUpdateInformationRequestDto) ;
	List< CustomerResponse> toResponseList (List<Customer> customers) ;
}
