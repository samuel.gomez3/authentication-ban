package co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table (name = "Customer")
public class CustomerEntity {
	
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String fullName;
	private String customer_key;
	private String country;
	private Double income;
	private String city;
	private int age;

}
