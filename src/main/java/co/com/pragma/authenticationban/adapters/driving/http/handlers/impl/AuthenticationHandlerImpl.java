package co.com.pragma.authenticationban.adapters.driving.http.handlers.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerRegistrationRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerUpdateInformationRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.request.LoginRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.response.CustomerResponse;
import co.com.pragma.authenticationban.adapters.driving.http.handlers.IAuthenticationHandler;
import co.com.pragma.authenticationban.adapters.driving.http.mappers.IAutherizationRequestMapper;
import co.com.pragma.authenticationban.domain.api.IAuthenticationServicePort;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
@RequiredArgsConstructor
public class AuthenticationHandlerImpl implements IAuthenticationHandler {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationHandlerImpl.class);

	private final IAuthenticationServicePort authenticationServicePort;
	private final IAutherizationRequestMapper autherizationRequestMapper;

	@Override
	public String login(LoginRequestDto loginRequestDto) {
		try {
			logger.info("Attempting to log in with username: {}", loginRequestDto.getUsername());
			return authenticationServicePort.login(loginRequestDto.getUsername(), loginRequestDto.getPassword());
		} catch (Exception e) {
			logger.error("Login failed: {}", e.getMessage());
			throw new RuntimeException("Failed to login", e);
		}
	}

	@Override
	public void customerRegistration(CustomerRegistrationRequestDto customerRegistrationRequestDto) {
		try {
			logger.info("Registering customer");
			authenticationServicePort
					.customerRegistration(autherizationRequestMapper.toCustomer(customerRegistrationRequestDto));
			logger.info("Customer registered successfully");
		} catch (Exception e) {
			logger.error("Customer registration failed: {}", e.getMessage());
			throw new RuntimeException("Failed to register customer", e);
		}
	}

	@Override
	public List<CustomerResponse> getCustomers() {
		try {
			logger.info("Fetching all customers");
			return autherizationRequestMapper.toResponseList(authenticationServicePort.getCustomers());
		} catch (Exception e) {
			logger.error("Failed to fetch customers: {}", e.getMessage());
			throw new RuntimeException("Failed to fetch customers", e);
		}
	}

	@Override
	public List<CustomerResponse> getCustomers(long id) {
		try {
			logger.info("Fetching customers with ID: {}", id);
			return autherizationRequestMapper.toResponseList(authenticationServicePort.getCustomers(id));
		} catch (Exception e) {
			logger.error("Failed to fetch customers by ID: {}", e.getMessage());
			throw new RuntimeException("Failed to fetch customers by ID", e);
		}
	}

	@Override
	public void updateCustomerInformation(CustomerUpdateInformationRequestDto updateInformationRequestDto, long id) {
		try {
			logger.info("Updating customer information for ID: {}", id);
			authenticationServicePort
					.updateCustomerInformation(autherizationRequestMapper.toCustomer(updateInformationRequestDto), id);
			logger.info("Customer information updated successfully for ID: {}", id);
		} catch (Exception e) {
			logger.error("Failed to update customer information: {}", e.getMessage());
			throw new RuntimeException("Failed to update customer information", e);
		}
	}

}
