package co.com.pragma.authenticationban.adapters.driven.jpa.mysql.adapter;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities.BusinessUserEntity;
import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.mappers.IAuthenticationEntityMapper;
import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.repositories.IBusinessUserRepository;
import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.repositories.ICustomerRepository;
import co.com.pragma.authenticationban.domain.model.Customer;
import co.com.pragma.authenticationban.domain.spi.IAuthenticationPersistencePort;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AuthentitcationMysqlAdapter implements IAuthenticationPersistencePort {

	private static final Logger logger = LoggerFactory.getLogger(AuthentitcationMysqlAdapter.class);

	private final IBusinessUserRepository businessUserRepository;
	private final ICustomerRepository customerRepository;
	private final IAuthenticationEntityMapper authenticationEntityMapper;

	@Override
	public BusinessUserEntity findByUsername(String username) {
		try {
			logger.info("Fetching business user by username: {}", username);
			return businessUserRepository.findByUsername(username);
		} catch (Exception e) {
			logger.error("Error occurred while fetching business user by username: {}", e.getMessage());
			throw new RuntimeException("Failed to find business user by username", e);
		}
	}

	@Override
	public void saveCustomer(Customer customer) {
		try {
			logger.info("Saving customer");
			customerRepository.save(authenticationEntityMapper.toEntity(customer));
			logger.info("Customer saved successfully");
		} catch (Exception e) {
			logger.error("Error occurred while saving customer: {}", e.getMessage());
			throw new RuntimeException("Failed to save customer", e);
		}
	}

	@Override
	public List<Customer> getCustomers() {
		try {
			logger.info("Fetching all customers");
			return authenticationEntityMapper.toModelList(customerRepository.findAll());
		} catch (Exception e) {
			logger.error("Error occurred while fetching all customers: {}", e.getMessage());
			throw new RuntimeException("Failed to fetch all customers", e);
		}
	}

	@Override
	public List<Customer> getCustomers(long id) {
		try {
			logger.info("Fetching customers by id: {}", id);
			return authenticationEntityMapper.toModelList(customerRepository.findById(id));
		} catch (Exception e) {
			logger.error("Error occurred while fetching customers by id: {}", e.getMessage());
			throw new RuntimeException("Failed to fetch customers by id", e);
		}
	}

	@Override
	public Customer findCustomerById(long id) {
		try {
			logger.info("Fetching customer by id: {}", id);
			List<Customer> customers = authenticationEntityMapper.toModelList(customerRepository.findById(id));

			if (customers.isEmpty()) {
				throw new RuntimeException("Customer not found");
			}

			return customers.get(0);
		} catch (Exception e) {
			logger.error("Error occurred while fetching customer by id: {}", e.getMessage());
			throw new RuntimeException("Failed to fetch customer by id", e);
		}
	}

}
