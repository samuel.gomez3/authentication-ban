package co.com.pragma.authenticationban.adapters.driving.http.mappers.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerRegistrationRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerUpdateInformationRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.dto.response.CustomerResponse;
import co.com.pragma.authenticationban.adapters.driving.http.mappers.IAutherizationRequestMapper;
import co.com.pragma.authenticationban.domain.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component
public class AuthenticationRequestMapper implements IAutherizationRequestMapper {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationRequestMapper.class);
	private static final int DEFAULT_ID = 0;
	private static final double DEFAULT_INCOME = 0.0;

	@Override
	public Customer toCustomer(CustomerRegistrationRequestDto customerRegistrationRequestDto) {
		logger.info("Mapping CustomerRegistrationRequestDto to Customer model");
		return new Customer(DEFAULT_ID, customerRegistrationRequestDto.getFullName(),
				customerRegistrationRequestDto.getKey(), customerRegistrationRequestDto.getCountry(), DEFAULT_INCOME,
				null, 0);
	}

	@Override
	public List<CustomerResponse> toResponseList(List<Customer> customers) {
		logger.info("Mapping list of Customer models to CustomerResponse");
		return customers.stream().map(c -> new CustomerResponse(c.getId(), c.getFullName(), c.getCountry(),
				c.getIncome(), c.getCity(), c.getAge())).collect(Collectors.toList());
	}

	@Override
	public Customer toCustomer(CustomerUpdateInformationRequestDto customerUpdateInformationRequestDto) {
		logger.info("Mapping CustomerUpdateInformationRequestDto to Customer model");
		return new Customer(DEFAULT_ID, null, null, null, customerUpdateInformationRequestDto.getIncome(),
				customerUpdateInformationRequestDto.getCity(), customerUpdateInformationRequestDto.getAge());
	}

}
