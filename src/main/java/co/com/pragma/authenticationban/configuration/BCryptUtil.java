package co.com.pragma.authenticationban.configuration;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for hashing and verifying passwords using BCrypt.
 */
public class BCryptUtil {
    
    private static final Logger logger = LoggerFactory.getLogger(BCryptUtil.class);

    /**
     * Hash a plain text password using BCrypt.
     *
     * @param plainTextPassword The plain text password to hash.
     * @return A hashed version of the password.
     */
    public static String hashPassword(String plainTextPassword){
        logger.info("Hashing password");
        return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
    }

    /**
     * Check if a plain text password matches a hashed password.
     *
     * @param plainPassword The plain text password.
     * @param hashedPassword The hashed password to compare to.
     * @return True if they match, false otherwise.
     */
    public static boolean checkPassword(String plainPassword, String hashedPassword) {
        logger.info("Verifying password");
        return BCrypt.checkpw(plainPassword, hashedPassword);
    }
}

