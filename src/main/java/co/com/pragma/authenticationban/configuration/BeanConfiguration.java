package co.com.pragma.authenticationban.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.adapter.AuthentitcationMysqlAdapter;
import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.mappers.IAuthenticationEntityMapper;
import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.repositories.IBusinessUserRepository;
import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.repositories.ICustomerRepository;
import co.com.pragma.authenticationban.domain.api.IAuthenticationServicePort;
import co.com.pragma.authenticationban.domain.spi.IAuthenticationPersistencePort;
import co.com.pragma.authenticationban.domain.useCase.AuthenticationUseCase;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
	private final IBusinessUserRepository businessUserRepository;
	private final ICustomerRepository customerRepository;
	private final IAuthenticationEntityMapper authenticationEntityMapper;

	@Bean
	public IAuthenticationPersistencePort authenticationPersistencePort() {
		return new AuthentitcationMysqlAdapter(businessUserRepository, customerRepository, authenticationEntityMapper);
	}
	
	@Bean
	public IAuthenticationServicePort authenticationServicePort() {
		
		return new AuthenticationUseCase(authenticationPersistencePort());
	}

}
