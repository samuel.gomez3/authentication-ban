package co.com.pragma.authenticationban;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthenticationBanApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationBanApplication.class, args);
	}

}
