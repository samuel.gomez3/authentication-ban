package co.com.pragma.authenticationban.domain.useCase;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import org.mindrot.jbcrypt.BCrypt;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities.BusinessUserEntity;
import co.com.pragma.authenticationban.configuration.BCryptUtil;
import co.com.pragma.authenticationban.configuration.JwtUtil;
import co.com.pragma.authenticationban.configuration.exceptions.CustomerNotFoundException;
import co.com.pragma.authenticationban.configuration.exceptions.UnauthorisedException;
import co.com.pragma.authenticationban.domain.api.IAuthenticationServicePort;
import co.com.pragma.authenticationban.domain.model.Customer;
import co.com.pragma.authenticationban.domain.spi.IAuthenticationPersistencePort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;



@Slf4j
@RequiredArgsConstructor
public class AuthenticationUseCase implements IAuthenticationServicePort {

	private static final String BUSINESS_ROLE = "business";

	private final IAuthenticationPersistencePort authenticationPersistencePort;

	@Override
	public String login(String username, String password) {
		log.info("Attempting to authenticate user: {}", username);

		BusinessUserEntity user = authenticationPersistencePort.findByUsername(username);
		if (user == null || !BCryptUtil.checkPassword(password, user.getPassword())) {
			log.error("Unauthorized access for user: {}", username);
			throw new UnauthorisedException("Unauthorized");
		}

		Map<String, Object> claims = new HashMap<>();
		claims.put("rol", BUSINESS_ROLE);

		return JwtUtil.generateToken(username, claims); 
	}

	@Override
	public void customerRegistration(Customer customer) {
		customer.setKey(BCrypt.hashpw(customer.getKey(), BCrypt.gensalt()));
		authenticationPersistencePort.saveCustomer(customer);
	}

	@Override
	public List<Customer> getCustomers() {
		return authenticationPersistencePort.getCustomers();
	}

	@Override
	public List<Customer> getCustomers(long id) {
		return authenticationPersistencePort.getCustomers(id);
	}

	@Override
	public void updateCustomerInformation(Customer updatedCustomer, long id) {
		Customer existingCustomer = authenticationPersistencePort.findCustomerById(id);
		if (existingCustomer == null) {
			throw new CustomerNotFoundException("Customer not found");
		}
		existingCustomer.setIncome(updatedCustomer.getIncome());
		existingCustomer.setCity(updatedCustomer.getCity());
		existingCustomer.setAge(updatedCustomer.getAge());
		authenticationPersistencePort.saveCustomer(existingCustomer);
	}

}
