package co.com.pragma.authenticationban.domain.spi;

import java.util.List;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities.BusinessUserEntity;
import co.com.pragma.authenticationban.domain.model.Customer;

public interface IAuthenticationPersistencePort {
	
	BusinessUserEntity findByUsername(String username);
	
	void saveCustomer(Customer customer);
	
	List<Customer> getCustomers();
	
	List<Customer> getCustomers(long id);
	
	Customer findCustomerById (long id);


}
