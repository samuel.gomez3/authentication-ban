package co.com.pragma.authenticationban.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
	
	private long id;
	private String fullName;
	private String key;
	private String country;
	private Double income;
	private String city;
	private int age;

}
