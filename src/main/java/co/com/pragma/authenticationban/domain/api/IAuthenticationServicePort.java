package co.com.pragma.authenticationban.domain.api;

import java.util.List;

import co.com.pragma.authenticationban.domain.model.Customer;

public interface IAuthenticationServicePort {

	String login(String username, String password);
	
	void customerRegistration(Customer customer);
	
	void updateCustomerInformation(Customer customer,long id);
	
	List<Customer> getCustomers();
	
	List<Customer> getCustomers(long id);
}
