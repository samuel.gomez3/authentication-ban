package co.com.pragma.authenticationban;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;

import org.springframework.test.web.servlet.MockMvc;

import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.entities.CustomerEntity;
import co.com.pragma.authenticationban.adapters.driven.jpa.mysql.mappers.impl.AuthenticationEntityMapper;
import co.com.pragma.authenticationban.adapters.driving.http.controller.AuthenticationController;
import co.com.pragma.authenticationban.adapters.driving.http.dto.request.LoginRequestDto;
import co.com.pragma.authenticationban.adapters.driving.http.handlers.IAuthenticationHandler;
import co.com.pragma.authenticationban.adapters.driving.http.mappers.IAutherizationRequestMapper;
import co.com.pragma.authenticationban.configuration.BCryptUtil;
import co.com.pragma.authenticationban.configuration.JwtUtil;

import co.com.pragma.authenticationban.domain.api.IAuthenticationServicePort;
import co.com.pragma.authenticationban.domain.model.Customer;
import co.com.pragma.authenticationban.domain.spi.IAuthenticationPersistencePort;
import co.com.pragma.authenticationban.domain.useCase.AuthenticationUseCase;
import io.jsonwebtoken.Claims;
import co.com.pragma.authenticationban.adapters.driving.http.dto.request.CustomerRegistrationRequestDto;

import static org.junit.jupiter.api.Assertions.assertThrows;

import io.jsonwebtoken.JwtException;


@WebMvcTest(AuthenticationController.class)
@ExtendWith(MockitoExtension.class)
class AuthenticationBanApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@InjectMocks
	private AuthenticationController authenticationController;

	@MockBean
	private IAuthenticationHandler authenticationHandler;

	private AuthenticationEntityMapper mapper;

	@Mock
	private IAuthenticationServicePort authenticationServicePort;

	@Mock
	private IAutherizationRequestMapper autherizationRequestMapper;

	@Mock
	private IAuthenticationPersistencePort authenticationPersistencePort;

	@InjectMocks
	private AuthenticationUseCase authenticationUseCase;

	@BeforeEach
	void setUp() {
		mapper = new AuthenticationEntityMapper();
		MockitoAnnotations.openMocks(this);

	}

	@Test
	public void testLogin() throws Exception {
		LoginRequestDto loginRequestDto = new LoginRequestDto("username", "password");
		when(authenticationHandler.login(any())).thenReturn("someToken");

		mockMvc.perform(post("/auth/login").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(loginRequestDto))).andExpect(status().isOk())
				.andExpect(content().string("someToken"));
	}

	@Test
	public void testCustomerRegistration() throws Exception {
		CustomerRegistrationRequestDto registrationDto = new CustomerRegistrationRequestDto("fullName", "key",
				"country");

		doNothing().when(authenticationHandler).customerRegistration(any());

		mockMvc.perform(post("/auth/customerRegistration").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(registrationDto))).andExpect(status().isOk())
				.andExpect(content().string("Your registration was successful"));
	}

	@Test
	void testHashPassword() {
		String plainPassword = "myPassword";
		String hashedPassword = BCryptUtil.hashPassword(plainPassword);

		assertTrue(hashedPassword != null && !hashedPassword.isEmpty());
		assertFalse(plainPassword.equals(hashedPassword));
	}

	@Test
	void testCheckPass() {
		String plainPassword = "myPassword";
		String hashedPassword = BCryptUtil.hashPassword(plainPassword);
		assertTrue(BCryptUtil.checkPassword(plainPassword, hashedPassword));
		assertFalse(BCryptUtil.checkPassword("wrongPassword", hashedPassword));
	}

	@Test
	void testGenerateToken() {
		Map<String, Object> claims = new HashMap<>();
		claims.put("role", "user");
		String username = "john.doe";

		String token = JwtUtil.generateToken(username, claims);

		assertNotNull(token);
	}

	@Test
	void testValidateToken() {
		Map<String, Object> claims = new HashMap<>();
		claims.put("role", "user");
		String username = "john.doe";

		String token = JwtUtil.generateToken(username, claims);

		Claims parsedClaims = JwtUtil.validateToken(token);

		assertEquals(username, parsedClaims.getSubject());
		assertEquals("user", parsedClaims.get("role"));
	}

	@Test
	public void testValidateToken_InvalidToken() {
		String invalidToken = "invalidToken";
		assertThrows(JwtException.class, () -> JwtUtil.validateToken(invalidToken));
	}

	@Test
	public void testValidateToken_NullToken() {
		String nullToken = null;
		assertThrows(IllegalArgumentException.class, () -> JwtUtil.validateToken(nullToken));
	}

	@Test
	public void testValidateToken_EmptyToken() {
		String emptyToken = "";
		assertThrows(IllegalArgumentException.class, () -> JwtUtil.validateToken(emptyToken));
	}

	@Test
	void testToEntity() {
		Customer customer = new Customer(1L, "John Doe", "key123", "USA", 1000.0, "New York", 30);
		CustomerEntity entity = mapper.toEntity(customer);

		assertEquals(1L, entity.getId());
		assertEquals("John Doe", entity.getFullName());
		assertEquals("key123", entity.getCustomer_key());
		assertEquals("USA", entity.getCountry());
		assertEquals(1000.0, entity.getIncome());
		assertEquals("New York", entity.getCity());
		assertEquals(30, entity.getAge());
	}

	@Test
	void testToModel() {
		CustomerEntity entity = new CustomerEntity(1L, "John Doe", "key123", "USA", 1000.0, "New York", 30);
		Customer customer = mapper.toModel(entity);

		assertEquals(1L, customer.getId());
		assertEquals("John Doe", customer.getFullName());
		assertEquals("key123", customer.getKey());
		assertEquals("USA", customer.getCountry());
		assertEquals(1000.0, customer.getIncome());
		assertEquals("New York", customer.getCity());
		assertEquals(30, customer.getAge());
	}

	@Test
	void testToModelList() {
		CustomerEntity entity1 = new CustomerEntity(1L, "John Doe", "key123", "USA", 1000.0, "New York", 30);
		CustomerEntity entity2 = new CustomerEntity(2L, "Jane Doe", "key456", "Canada", 1500.0, "Toronto", 40);

		List<CustomerEntity> entities = Arrays.asList(entity1, entity2);
		List<Customer> customers = mapper.toModelList(entities);

		assertEquals(2, customers.size());

		assertEquals(1L, customers.get(0).getId());
		assertEquals("John Doe", customers.get(0).getFullName());
		assertEquals("key123", customers.get(0).getKey());
		assertEquals("USA", customers.get(0).getCountry());
		assertEquals(1000.0, customers.get(0).getIncome());
		assertEquals("New York", customers.get(0).getCity());
		assertEquals(30, customers.get(0).getAge());

		assertEquals(2L, customers.get(1).getId());
		assertEquals("Jane Doe", customers.get(1).getFullName());
		assertEquals("key456", customers.get(1).getKey());
		assertEquals("Canada", customers.get(1).getCountry());
		assertEquals(1500.0, customers.get(1).getIncome());
		assertEquals("Toronto", customers.get(1).getCity());
		assertEquals(40, customers.get(1).getAge());
	}

}
